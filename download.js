const groceryList = [
  "oeuf",
  "Lait",
  "jambon"
]

const testList = [
["Ce jeu est vraiment net!", "Denis Talbot", "thumbnail-0.jpg","2019-01-18", "Destiny 2", "MMO", "7/10"],
["Meilleur MMO, pour vrai", "Pabhi Aisé", "thumbnail-1.jpg","2020-02-24", "Elder Scrolls Online", "MMO", "9/10"],
["Ayoye, on est tombé de haut", "Chupa Quave", "thumbnail-2.jpg", "2019-11-21", "World Of Warcraft", "MMO", "4/10"],
["Encore pertinent en 2020", "Jout Usel", "thumbnail-3.jpg", "2020-03-02", "The Elder Scrolls V: Skyrim", "RPG", "10/10"],
["Vous allez pleurer", "Emo Tiff", "thumbnail-4.jpg", "2019-08-02", "The Witcher 3: Wild Hunt", "RPG", "10/10"],
["Un bon entraînement", "Imam Pahafass", "thumbnail-5.jpg", "2020-01-25", "Call of Duty: Modern Warfare", "FPS", "7/10"],
["Trop rapide pour moi!", "Paco Réen", "thumbnail-6.jpg", "2019-04-27", "Starcraft II", "RTS", "8/10"],
["Bim Bam Boum! Haha", "Pafoul Briant", "thumbnail-7.jpg", "2018-10-20", "Battlefield V", "FPS", "10/10"],
["Pour les gens de culture", "Élie Tiss", "thumbnail-8.jpg", "2019-09-11", "PlanetSide 2", "FPS", "10/10"],
["Old But Gold", "Han Glaise", "thumbnail-9.jpg", "2019-07-03", "GoldenEye 007", "FPS", "9/10"],
["Pardon?", "Kose Sow", "thumbnail-10.jpg", "2020-03-08", "Warcraft 3: Reforged", "RTS", "3/10"],
["Bon quand t'es pas un casual", "Tressel Itist", "thumbnail-11.jpg", "2020-02-24", "Dota 2", "RTS", "9/10"]
]

const commentaires = [
]

export function getTest(req, res) {
  const id = req.params.id
  res.send(testList[id])
}

export function getTestData(req,res) {
  res.send("a");
}

export function putGroceryList(req, res) {
  const id = req.params.id || null
  const name = req.body.name || "Random"
  const maxGroceryList = groceryList.length;

  if(id && id < maxGroceryList) {
    console.log(`update value ${groceryList[id]} as ${name}`)
    groceryList[id] = name;
    res.send(groceryList);
    return;
  }

  groceryList.push(name);
  res.send(groceryList)
}

export function deleteGroceryList(req, res) {
  const id = req.params.id || null
  const maxGroceryList = groceryList.length;

  if(!id || id >= maxGroceryList) {
    res.status(404).send(groceryList);
    return;
  }

  let removed = groceryList.splice(id, 1);
  res.send(groceryList)
}