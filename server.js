import express from "express"
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './router.js'

const app = express();
const PORT = 3000;

const testList = [
  ["Ce jeu est vraiment net!", "Denis Talbot", "thumbnail-0.jpg","2019-01-18", "Test0", "Destiny 2", "MMO", "7/10"],
  ["Meilleur MMO, pour vrai", "Pahbi Aisé", "thumbnail-1.jpg","2020-02-24", "Test1", "Elder Scrolls Online", "MMO", "9/10"],
  ["On est tombé de haut", "Chupa Quave", "thumbnail-2.jpg", "2019-11-21", "Test2", "World Of Warcraft", "MMO", "4/10"],
  ["Encore pertinent en 2020", "Jout Usel", "thumbnail-3.jpg", "2020-03-02", "Test3", "The Elder Scrolls V: Skyrim", "RPG", "10/10"],
  ["Vous allez pleurer", "Emo Tiff", "thumbnail-4.jpg", "2019-08-02", "Test4", "The Witcher 3: Wild Hunt", "RPG", "10/10"],
  ["Un bon entraînement", "Imam Pahafass", "thumbnail-5.jpg", "2020-01-25", "Test5", "Call of Duty: Modern Warfare", "FPS", "7/10"],
  ["Trop rapide pour moi!", "Paco Réen", "thumbnail-6.jpg", "2019-04-27", "Test6", "Starcraft II", "RTS", "8/10"],
  ["Bim Bam Boum! Haha", "Pafoul Briant", "thumbnail-7.jpg", "2018-10-20", "Test7", "Battlefield V", "FPS", "10/10"],
  ["Pour les gens de culture", "Élie Tiss", "thumbnail-8.jpg", "2019-09-11", "Test8", "PlanetSide 2", "FPS", "10/10"],
  ["Old But Gold", "Han Glaise", "thumbnail-9.jpg", "2019-07-03", "Test9", "GoldenEye 007", "FPS", "9/10"],
  ["Pardon?", "Kose Sow", "thumbnail-10.jpg", "2020-03-08", "Test10", "Warcraft 3: Reforged", "RTS", "3/10"],
  ["Quand t'es pas un casual", "Tressel Itist", "thumbnail-11.jpg", "2020-02-24", "Test11", "Dota 2", "RTS", "9/10"]
  ]
  
  /*const commentaires = [
    //Jeu 1:
    [["Jean-Paul", "jpt@caramail.com", "T'as ben raison toé!"], ["Roger", "bigrog@hotmail.com", "C'est vrai s'qui dit l'autre!"]],

    //Jeu 2:
    [["Pierre", "peteindahouse@gmail.com", "C'est pas faux"]],

    //Jeu 3:
    [],

    //Jeu 4:
    [],

    //Jeu 5:
    [],

    //Jeu 6:
    [],

    //Jeu 7:
    [],

    //Jeu 8:
    [],

    //Jeu 9:
    [],

    //Jeu 10:
    [],

    //Jeu 11:
    [],

    //Jeu 12:
    [["Mathieu C.", "mattyc@outlook.com", "Mettons ouin"]]

    
  ] */

const commentaires = [
  {
    idArticle: 0,
    comments: [
      {
        index: 1,
        username: "Jean-Paul",
        email: "jpt@caramail.com",
        text: "T'as ben raison toé!"
      },
      {
        index: 2,
        username: "Roger",
        email: "bigrog@hotmail.com",
        text: "C'est vrai s'qui dit l'autre!"
      }
    ]
  },

  {
    idArticle: 1,
    comments: [
      {
        index: 1,
        username: "Pierre",
        email: "peteindahouse@gmail.com",
        text: "C'est pas faux!"
      }
    ]
  },

  {
    idArticle: 2,
    comments:[]
  },
  
  {
    idArticle: 3,
    comments:[]
  },

  {
    idArticle: 4,
    comments:[]
  },

  {
    idArticle: 5,
    comments:[]
  },

  {
    idArticle: 6,
    comments:[]
  },

  {
    idArticle: 7,
    comments:[]
  },

  {
    idArticle: 8,
    comments:[]
  },

  {
    idArticle: 9,
    comments:[]
  },

  {
    idArticle: 10,
    comments:[]
  },

  {
    idArticle: 11,
    comments:[
      {
        index: 1,
        username: "Mathieu C.",
        email: "mattyc@outlook.com",
        text: "Mettons ouin..."
      }
    ]
  },

]

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.get('/', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.get('/games', (req, res) => {
  const mx = req.params;
  console.log(mx);

  testList.sort(function(a,b) {
    a = new Date(a[3]);
    b = new Date(b[3]);
    return a>b ? -1 : a<b ? 1 : 0;
  });
  res.send(testList)
});

app.get('/game/:id', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send(testList[req.params.id]);
});

app.put('/game', (req, res) => {
  const mx = req.params;
  console.log(mx);

  let test = [req.body.titre, req.body.auteur, req.body.image, req.body.date, req.body.texte, req.body.nom, req.body.categorie, req.body.note];
  testList.push(test);
  res.send("Hello world");
});

app.get('/game/:id/comments', (req, res) => {
  const mx = req.params;
  console.log(mx);

  res.send(commentaires[req.params.id].comments);
});

app.put('/game/:id/comment', (req, res) => {
  const mx = req.params;
  console.log(mx);

  let nouveauComm = {
    index: req.body.index,
    username: req.body.username,
    email: req.body.email,
    text: req.body.text
  }

  commentaires[req.params.id].comments.push(nouveauComm);

  res.send("Hello world");
});

app.delete('/games/:id/comment/:id', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

//app.get('/article/:id', (req, res) => {
//  const params = req.params;
//  res.send(params);
//});

//app.get('/article', (req, res) => {
//  const params = req.quey;
//  res.send(params);
//});

routes(app);

//app.post('/truc', (req, res) => {
//  const m = req.body;
//  console.log(m);
//  res.send(m);
//});

app.use((req, res) => {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(PORT, () =>
  console.log(`Node server running on ${PORT}!`),
);