import {getTest, getTestData, putGroceryList, deleteGroceryList} from "./download.js"

const routes = (app) => {
  app.route('/game/:id')
      .get(getTest)
  app.route('/games')
      .get(getTestData)
  app.route('/game')
      .put()
  app.route('/game/:id/comments')
      .get()
  app.route('/game/:id/comment')
      .put()
  app.route('/game/comment/:id/comment/:id')
      .delete()
}

export default routes